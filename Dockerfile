FROM python:bullseye

RUN pip install pyminizip

WORKDIR /

COPY src src

COPY cmd cmd

# RUN echo 'alias kachow="python3 ./src/helloworld2.py"' >> ~/.bashrc

RUN chmod -R u+x /cmd

RUN echo 'export PATH=/cmd:$PATH' >> ~/.bashrc

RUN . ~/.bashrc
