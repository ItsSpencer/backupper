# backupper

`docker build . -t backupper`



Windows:
`docker run --env-file .\.env -v /:/hostvolumes -v .\config:/config -v .\output:/output backupper /cmd/backup`
`docker run --env-file .\.env -v /:/hostvolumes -v .\config:/config -v .\output:/output -it backupper bash`

Linux:
`docker run --env-file ./.env -v /:/hostvolumes -v ./config:/config -v ./output:/output backupper /cmd/backup`
`docker run --env-file ./.env -v /:/hostvolumes -v ./config:/config -v ./output:/output -it backupper bash`

Development (Windows):
`docker run --env-file .\.env -v \:/hostvolumes -v .\config:/config -v .\output:/output -v .\src:/src -it backupper bash`
`/cmd/backup`

Copy from VPS (Windows):
`scp -P XXX -r YYY@ZZZ:/opt/backupper/output/ .`

Copy from VPS (Linux, only new archives):
`rsync --port XXX -rvu YYY:/ZZZ/* .` 