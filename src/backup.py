import sys
sys.path.append('../')

import os
import datetime

import pyminizip

from config import directories
import shutil
from distutils.dir_util import copy_tree


print("Encryption key: %s"%(os.environ['SYMMETRIC_ENCRYPTION_KEY']))

print("Compression strength: %s"%(os.environ['COMPRESSION_STRENGTH']))

print("Zipping the directories...")

for directory in directories.directories:
    print(directory+"...", flush=True)
    try:
        shutil.copytree('/hostvolumes/%s'%(directory), '/tmp/backupper/directories/%s'%(directory),dirs_exist_ok=True,ignore_dangling_symlinks=True)
    except OSError as err:
        print(err)

format="zip"
os.makedirs("/tmp/backupper/directories",exist_ok=True)
name = 'archive_%s'%(datetime.datetime.now().isoformat()).replace(":","_")
shutil.make_archive('/tmp/backupper/%s_unencrypted'%(name), format, "/tmp/backupper/directories")

print("Compressing and encrypting the directories...")
pyminizip.compress('/tmp/backupper/%s_unencrypted.%s'%(name,format), None, '/tmp/backupper/%s.%s'%(name,format), os.environ['SYMMETRIC_ENCRYPTION_KEY'], int(os.environ['COMPRESSION_STRENGTH'])) # compression: 0-9 (0 no, 1 low, 9 high)
shutil.move('/tmp/backupper/%s.%s'%(name,format), "/output")